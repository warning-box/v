### 📦&ensp;warningBox()

- **Description:** `warningBox()` is a plugin that displays a warning box on page load, utilizing `localStorage` to set a custom time period before it's displayed to the same user again. Can be used as a _terms of use_ or _privacy consent_ modal, or similar.
- **Requirements:** intermediate HTML/CSS knowledge.
- **Features:**
    - User must click the specified `button` to dismiss the box.  
      (supports <kbd>TAB</kbd> and <kbd>Enter</kbd> keys too!)
    - User's session time starts after the exit button is clicked. After the session is over, the box will reappear when the user next visits the page.
- **Author:** HT (@ glenthemes)

---

#### Preview:

![Screen recording GIF of a user visiting the page. After a brief delay, a warning box fades into view, covering the main contents of the page. The user then clicks the purple "close" button to dismiss the box, and it fades out and makes the main page content available again.](https://github.com/ht-devx/ht-devx.github.io/assets/162752471/625bc0a5-a58b-44e4-8ca4-c65f88b234cd)

💛 [warning-box.gitlab.io/v/index.html](https://warning-box.gitlab.io/v/index.html)

#### Demo code:
🧀 [jsfiddle.net/glenthemes/gp8e1fm6/](https://jsfiddle.net/glenthemes/gp8e1fm6/)

---

#### How to Install:
I've included the bare-bones of what's required for it to work. You can add any styling you want.

Let's set up the warning box markup first (HTML).  
Paste this under `<body>` (preferably the very first thing that's inside body):
```html
<!--- warning box content --->
<div id="warning-box">
  <div id="box-inner">
    <!-- box heading -->
    <h2>some header here</h2>
    
    <!-- box text content -->
	<div id="box-text">
      <p>
	  	Some text here
      </p>
    </div><!--end box-text-->
    
    <!-- box exit button -->
    <div id="box-button-wrapper">
      <button>close</button>
    </div>    
  </div><!--end box-inner-->
</div><!--end warning-box-->
```
Now for the actual functionality.  
Paste the following under `<head>` or before `</head>`:
```html
<!--✻✻✻✻✻✻  warning box by @glenthemes  ✻✻✻✻✻✻-->
<script src="https://warning-box.gitlab.io/v/init.js"></script>
<script>
warningBox({
  boxID: "#warning-box",
  buttonID: "#box-button-wrapper",
  delay: 0, // delay before the warning box appears (seconds)
  fadeIn: 0.8, // fade-in transition (seconds)
  fadeOut: 0.4, // fade-out transition (seconds)
  showAgainAfter: 10, // show the warning box again after a set period of time (seconds)
  disableScrolling: true // boolean
})
</script>
<style warning-box>
/*---- WARNING BOX CONTAINER ----*/
#warning-box {
  position:fixed;
  top:0;left:0;
  width:100%;
  height:100vh;
  display:grid;
  place-items:center;
  
  backdrop-filter:blur(3px); /* background blur (optional) */
  z-index:100; /* increase this number if you can't see your box */
  
  /* don't remove next 2 lines */
  opacity:0;
  transition:opacity var(--Box-FadeIn-Speed) ease-in-out;
}

/* don't remove this section */
#warning-box.leave {
  transition:opacity var(--Box-FadeOut-Speed) ease-in-out;
}

/*---- WARNING BOX OVERLAY ----*/
#warning-box:before {
  content:"";
  position:absolute;
  top:0;left:0;
  width:100%;
  height:100%;
  background-color:#ffffff; /* overlay color */
  opacity:60%; /* overlay opacity */
  z-index:-1; /* don't edit or remove */
}

/*---- WARNING BOX ----*/
#box-inner {
  width:45vw;
}

#box-text {
  margin-top:1.3rem;
  margin-bottom:1.3rem;
  
  max-height:50vh; /* maximum height before a scrollbar appears */
  overflow:auto;
  
  /* next 2 lines are for scrollbar adjustments */
  padding-right:13px;
  margin-right:-13px; /* this should be negative of the above value */
}

/*---- EXIT WARNING BOX BUTTON CONTAINER ----*/
#box-button-wrapper {
  display:grid;
  justify-content:center;
}

/*---- EXIT WARNING BOX BUTTON ----*/
#box-button-wrapper button {
  cursor:pointer;
}
</style>
```

---

| Option Name | Explanation |
| ----------- | ----------- |
| `boxID` | Your warning box selector. |
| `buttonID` | Your selector for the trigger that exits the box (doesn't have to be a `button`) |
| `delay` | Delay (in `seconds`) before the box appears. |
| `fadeIn` | Fade-in transition duration (in `seconds`). |
| `fadeOut` | Fade-out transition duration (in `seconds`). |
| `showAgainAfter` | Customize the user's session time, aka how long you permit the user to access your page without showing the warning box (in `seconds`). |
| `disableScrolling` | Allow or prevent scrolling of the main page when the warning box is active (either `true` or `false`). |

---

#### Issues & Troubleshooting:
[discord.gg/RcMKnwz](https://discord.gg/RcMKnwz)

---

#### Found this useful?

Consider leaving me a tip over at [ko-fi.com/glenthemes](https://ko-fi.com/glenthemes)!  
Thank you ✨
