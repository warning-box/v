/*---------------------------------------------------------

    Warning box on page load by @glenthemes
    [#] gitlab.com/warning-box/v
    [#] v. beta - 2023-09-22
    [#] Last updated: 2023-09-22 2:24PM [PDT]
    
---------------------------------------------------------*/
window.warningBox = (b) => {
  
  let boxID = b.boxID;
  let buttonID = b.buttonID;
  let delay = b.delay;
  let fadeIn = b.fadeIn;
  let fadeOut = b.fadeOut;
  let session = b.showAgainAfter;
  let blockScroll = b.disableScrolling;
  
  let vs = [boxID, buttonID, delay, fadeIn, fadeOut, session, blockScroll];
  let vs_present = vs.every(v => typeof v !== "undefined");
  
  if(vs_present){
    
    boxID = boxID.toString().trim();
    buttonID = buttonID.toString().trim();
    delay = delay.toString().trim();
    fadeIn = fadeIn.toString().trim();
    fadeOut = fadeOut.toString().trim();
    session = session.toString().trim();
    blockScroll = blockScroll.toString().trim();
    
    let vs_has_content = vs.every(v => v !== "");
    
    if(vs_has_content){
      delay = Number(delay);
      fadeIn = Number(fadeIn);
      fadeOut = Number(fadeOut);
      session = Number(session);
      
      blockScroll = blockScroll == "true" || blockScroll == true || blockScroll == "yes";
      
      // spit error if vals aren't numbers
      if(isNaN(delay) || isNaN(fadeIn) || isNaN(fadeOut) || isNaN(session)){
        isNaN(delay) ? console.error("`delay` must be a number.") : ""
        isNaN(fadeIn) ? console.error("`fadeIn` must be a number.") : ""
        isNaN(fadeOut) ? console.error("`fadeOut` must be a number.") : ""
        isNaN(session) ? console.error("`showAgainAfter` must be a number.") : ""
      }
      
      else {
        let box = document.querySelector(boxID);
        let button = document.querySelector(buttonID);
        
        if(box && button){

          const warningBoxTimestamp = localStorage.getItem("warningBoxTimestamp");
          if(!warningBoxTimestamp || (Date.now() - warningBoxTimestamp > session*1000)){
            // run the box
            blockScroll ? document.querySelector("body").style.overflow = "hidden" : ""
            
            box.style.setProperty("--Box-FadeIn-Speed",`${fadeIn}s`);
            box.style.setProperty("--Box-FadeOut-Speed",`${fadeOut}s`);
            delay *= 1000;

            setTimeout(() => {
              box.style.opacity = "1"
            },delay);

            button.addEventListener("click", () => {
              if(!box.matches(".leave")){
                box.classList.add("leave");
                closeBox()
              }            
            })

            const closeBox = () => {
              box.style.opacity = "0";

              setTimeout(() => {
                box.remove();
                blockScroll ? document.querySelector("body").style.overflow = "" : ""
                
                // box was accessed
                localStorage.setItem("warningBoxTimestamp", Date.now());

                // bring the box back after session time has passed
                setTimeout(() => {
                  localStorage.removeItem("warningBoxTimestamp");
                },session*1000);
              },fadeOut*1000)
            }
          }
          
          // box is on cooldown, remove it
          else {
            box.remove()
          }
        }//end: if both $box and $button exist        
      }// end: if number vals are actually numbers      
    }//end: if args have stuff in them
  }//end: if all args are present
  
   else {
     console.error("All arguments must be present and non-empty.")
  }
}//end: warningBox function
